Dependencies:
  docker

To run:
  docker-compose up

Access web pannel
  http://localhost:15672/
  username: guest
  password: guest

RabbitMQ is acccessible on default 15672 port
  localhost:15672

Sample simple message sender placed into docker source:
  ./scheduler/app/scheduler.js
